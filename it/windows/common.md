
CLI
===

Copying files
-------------

- `xcopy c:\source\file.txt c:\destination\newfile.txt*` - adds star after filename to indicate that it is file (not directory) name 
and to prevent asking about object type ([source](http://stackoverflow.com/questions/4283312/batch-file-asks-for-file-or-folder))


Other
=====
- `change port /query` - просмотр доступных портов (в том числе проброшенных через RDP)