XSD
----

- [Convert XSD-schema to HTML Documentation](https://amreldib.com/blog/HowToTurnXmlSchemaXsdToDocumentation/)
by XSLT-stylesheet [xs3p](https://xml.fiforms.org/xs3p/) 
and [MSXSL](https://www.microsoft.com/en-us/download/details.aspx?id=21714) utility.