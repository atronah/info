Moved to [GitHub](https://github.com/atronah/info)
=================

Introduction
============
Welcome to my small virtual world.
I am atronah (yes, this word is from _TES III: Morrowind_),
and I am programmer or something like this.

This git project intends to collect all about my others projects and useful (at least for me) technical info in one place.
You can consider it as a hall of my programming world.


-----------------


About me
========
- Real name: Anufriev Artem
- Born: in 1990 in Ukhta town, Russia
- English level: elementary
- Skills: Python, Qt, MySQL, Firebird PSQL, C++, Git, SVN


-----------------



Projects
========

Personal
--------

- [atromio](https://github.com/atronah/atromio) - app (only idea at the moment) to organise data about money spending.
- [firebird_utils](https://github.com/atronah/firebird_utils) - useful script and others for working with Firebird dbms.
- [sql_deploy](https://github.com/atronah/sql_deploy) - tool to concatenate sql-scripts into single scripts in correct order (described in config file)


Work
----
My work projects are in private repositories.
But, if you have rights, you can find info about them [here](https://gitlab.com/mplus/info).


-----------------


Knowledge base
==============
A collection of useful information about different things

Index
-----
### IT
- apps
    - [7z](it/apps/7z.md)
- windows
    - [common](it/windows/common.md)
    - [batch](it/windows/batch.md)
- dbms
    - [mysql](it/dbms/mysql.md)
    - [firebird](it/dbms/firebird.md)
- vcs
    - [svn](it/vcs/svn.md)
    - [git](it/vcs/git.md)
- unix
    - [common](it/unix/common.md)
    - [fedora](it/unix/fedora.md)
    - [vim](it/unix/vim.md)
    - [ssh](it/unix/ssh.md)
    - [bash](it/unix/bash.md)
    - [vpn](it/unix/vpn.md)
- dev
    - [python](it/dev/python.md)
    - [xml](it/dev/xml.md)